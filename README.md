# Unattended VideoID

This is a demo application that showcase the use of the JavaScript client to record an **Unattended VideoID**. 
It contains a configured reverse proxy with **Nginx** and **Apache**, and a simple HTML and JavaScript code to run a VideoID. 

## How to run the demo
You need Docker running on your PC in order to run the demo. You will also need to edit the appropriate configuration file, 
depending on whether you want a Nginx or Apache server, and set your **Authentication Token**. 

### Building and Running a Nginx based image
Edit the file *config/nginx.conf* and substitute *YOUR_TOKEN_HERE* with your own token. Then build the image:

```sh
docker build -f docker/nginx/Dockerfile -t videoid-unattended-web-nginx .
docker run --name videoid-unattended-web-nginx -p 443:443 -d videoid-unattended-web-nginx
```

### Building and Running an Apache based image
Edit the file *config/apache.conf* and substitute *YOUR_TOKEN_HERE* with your own token. Then build the image:

```sh
docker build -f docker/apache/Dockerfile -t videoid-unattended-web-apache .
docker run --name videoid-unattended-web-apache -p 443:443 -d videoid-unattended-web-apache
```


After running the container you can access https://localhost on your PC and run the VideoID. Since HTTPS is a requirement for the VideoID, Nginx has been configured with a Self Signed Certificate. You will have to add an exception to the browser the first time you open the demo.