$(document).ready(function()
{
    var videoId = null;

    $("#startBtn").click(function()
    {
        $("#startBtn").attr("disabled", "disabled");

        EID.init({
            proxyEndpoint: 'https://localhost/eid-api'
        })
        .then(function() {

            videoId = EID.videoId("#video");
            return videoId.turnOn();
        })
        .then(function () {

            return videoId.start({
                idType: 62,
                minSimilarityLevel: "Low"
            });
        })
        .then(function (video) {

            videoId.turnOff();
            alert('New video!!! Id: ' + video.id);
        }, function() {
            alert('Failed video');
        });
    });
});